<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

         \App\Models\tipoUsuario::create([      
            'nombre'      => 'Administrador',
            'descripcion'     => 'Administrador',
        ]);

         \App\Models\tipoUsuario::create([      
            'nombre'      => 'Mensajero',
            'descripcion'     => 'Mensajero',
        ]);

         \App\Models\tipoUsuario::create([      
            'nombre'      => 'cliente',
            'descripcion'     => 'cliente',
        ]);

         \App\Models\User::factory(10)->create();


         \App\Models\productos::factory(4)->create();


    }
}
