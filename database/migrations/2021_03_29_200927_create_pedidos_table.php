<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cliente')->unsigned();
            $table->boolean('estado_pedido');
            $table->boolean('estado_pago');
            $table->bigInteger('id_mensajero')->unsigned();
            $table->bigInteger('id_administrador')->unsigned();

            $table->timestamps();

            $table->foreign('cliente')->references('id')->on('users');
            $table->foreign('id_mensajero')->references('id')->on('users');
            $table->foreign('id_administrador')->references('id')->on('users');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
