<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallePedido extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_pedido', 'id_producto','cantidad',
    ];

    public function Pedido(){
        return $this->belongsToMany(Pedido::class);  
    }

    public function Producto(){
        return $this->belongsToMany(Producto::class);  
    }
}
