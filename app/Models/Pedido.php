<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = [
        'cliente', 'estado_pedido','estado_pago','id_mensajero','id_administrador',
    ];

    public function DetallePedido(){
        return $this->hasMany(DetallePedido::class);
    }

    
    public function User(){
        return $this->belongsToMany(User::class);  
    }

    public function Mensajero(){
        return $this->belongsToMany(User::class);  
    }

    public function Administrador(){
        return $this->belongsToMany(User::class);  
    }

}
