<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'tipoUsuario',
        
    ];

    public function tipoUsuario(){
        return $this->belongsTo(tipoUsuario::class);  
    }

    public function Pedido(){
        return $this->hasMany(Pedido::class);  
    }

    public function Mensajero(){
        return $this->hasMany(Pedido::class);  
    }

    public function Administrador(){
        return $this->hasMany(Pedido::class);  
    }



    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
